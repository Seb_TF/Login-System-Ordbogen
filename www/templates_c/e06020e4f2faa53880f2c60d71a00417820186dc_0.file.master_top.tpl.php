<?php
/* Smarty version 3.1.33-dev-5, created on 2018-07-03 13:35:25
  from '/MAMP/htdocs/ordbogen/www/templates\master_top.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33-dev-5',
  'unifunc' => 'content_5b3b7b9de370b6_14154000',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e06020e4f2faa53880f2c60d71a00417820186dc' => 
    array (
      0 => '/MAMP/htdocs/ordbogen/www/templates\\master_top.tpl',
      1 => 1530624923,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b3b7b9de370b6_14154000 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<html lang="da">
<head>
  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11145887045b3b7b9de33e67_45936495', 'head');
?>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_1521263405b3b7b9de35796_00333076', 'javascript');
?>

  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2938907355b3b7b9de36715_66433366', 'css');
?>

</head>
<body>
  <div>
<?php }
/* {block 'head'} */
class Block_11145887045b3b7b9de33e67_45936495 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_11145887045b3b7b9de33e67_45936495',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <title>Login</title>
  <?php
}
}
/* {/block 'head'} */
/* {block 'javascript'} */
class Block_1521263405b3b7b9de35796_00333076 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'javascript' => 
  array (
    0 => 'Block_1521263405b3b7b9de35796_00333076',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

    <?php echo '<script'; ?>
 type="text/javascript" src="javascript/jsonrpc2.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="javascript/jquery-3.3.1.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="javascript/FormUtil.class.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="javascript/Admin.class.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="javascript/js/bootstrap.min.js"><?php echo '</script'; ?>
>
  <?php
}
}
/* {/block 'javascript'} */
/* {block 'css'} */
class Block_2938907355b3b7b9de36715_66433366 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'css' => 
  array (
    0 => 'Block_2938907355b3b7b9de36715_66433366',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
  <?php
}
}
/* {/block 'css'} */
}

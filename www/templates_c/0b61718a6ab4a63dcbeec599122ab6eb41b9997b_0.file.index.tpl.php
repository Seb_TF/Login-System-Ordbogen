<?php
/* Smarty version 3.1.33-dev-5, created on 2018-07-07 18:03:59
  from '/MAMP/htdocs/ordbogen/www/templates\index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33-dev-5',
  'unifunc' => 'content_5b41008f7123a9_99422837',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0b61718a6ab4a63dcbeec599122ab6eb41b9997b' => 
    array (
      0 => '/MAMP/htdocs/ordbogen/www/templates\\index.tpl',
      1 => 1530986614,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:login_form.tpl' => 1,
    'file:logout.tpl' => 1,
  ),
),false)) {
function content_5b41008f7123a9_99422837 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12326804735b41008f70a223_82741686', 'javascript');
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3603482595b41008f70af15_58330180', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, 'master.tpl');
}
/* {block 'javascript'} */
class Block_12326804735b41008f70a223_82741686 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'javascript' => 
  array (
    0 => 'Block_12326804735b41008f70a223_82741686',
  ),
);
public $append = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo '<script'; ?>
 type="text/javascript" src="javascript/Login.class.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'javascript'} */
/* {block 'content'} */
class Block_3603482595b41008f70af15_58330180 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_3603482595b41008f70af15_58330180',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<body onload="Login.onLoad()">
  <div class="login-content">
      <?php $_smarty_tpl->_subTemplateRender("file:login_form.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  </div>
  <div class="logout-content">
      <?php $_smarty_tpl->_subTemplateRender("file:logout.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
  </div>
  <div id="table-home">
    <div class="col-md-4 table-col"></div>
    <div class="login col-md-4 table-col">
      <table id="log" name="log">
        <thead>
          <th>Email</th>
          <th>Type</th>
        </thead>
        <tbody id="tbody-log">
        </tbody>
      </table>
    </div>
    <div class="col-md-4 table-col"></div>
  </div>
</body>
<?php
}
}
/* {/block 'content'} */
}

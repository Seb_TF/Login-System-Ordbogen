Login = {

  onLoad: function(){
    $('.logout-content').hide();
  },

  addUser: function(){
    var data = FormUtil.packForm('#home');

    Admin.rpc.invoke('Login.addUser', data, Login.addSuccess, Login.failure);
  },

  addSuccess: function(res){
    $('.login-content').hide("fast");
    $('.logout-content').show("fast");
    $('#log').append(res.html);
    alert(res.message);
  },

  logout: function(){
    var user = $('#tbody-log tr:last-child() td:first-child()').text();
    Admin.rpc.invoke('Login.logout', user, Login.logoutSuccess, Login.failure);
  },

  logoutSuccess: function(res){
    $('.login-content').show("fast");
    $('.logout-content').hide("fast");
    $('#log').append(res.html);
    alert(res.message);
  },

  failure: function(res){
    alert(res.message);
  }
}

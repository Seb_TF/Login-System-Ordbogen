Admin = {
  rpc: new JsonRpcClient('api/json.php'),

  notify: function(message, where, type, delay)
  {
    if (where == undefined)
      where = 'center';

    if (type == undefined)
      type = 'info';

    if (delay == undefined)
      delay = 10000;

    noty({
      layout: where,
      type: type,
      text: message,
      timeout: delay,
    });
  },

  failure: function(res)
  {
    Admin.notify(res.message, 'center', 'warning', 5000);
  },
}

<?php
namespace www;

error_reporting(E_ALL & ~E_NOTICE);

require_once($_SERVER['DOCUMENT_ROOT'].'/ordbogen/php/smarty/libs/Smarty.class.php');

$paths = [
  'here' => '.',
  'root' => realpath(__DIR__.'/../')
];
set_include_path(join(';', $paths));

spl_autoload_register(function ($className) {
    require_once $className . '.class.php';
});
$dsn = 'mysql:host=127.0.0.1';
$user = 'root';
$password = 'root';
$pdo = new \PDO($dsn, $user, $password);
\php\login\LoginSystem::setConnection($pdo);

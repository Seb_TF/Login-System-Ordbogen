<html lang="da">
<head>
  {block name='head'}
  <title>Login</title>
  {/block}
  {block name='javascript'}
    <script type="text/javascript" src="javascript/jsonrpc2.js"></script>
    <script type="text/javascript" src="javascript/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="javascript/FormUtil.class.js"></script>
    <script type="text/javascript" src="javascript/Admin.class.js"></script>
    <script type="text/javascript" src="javascript/js/bootstrap.min.js"></script>
  {/block}
  {block name='css'}
  <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/custom.css">
  {/block}
</head>
<body>
  <div>

{extends 'master.tpl'}
{block name='javascript' append}
<script type="text/javascript" src="javascript/Login.class.js"></script>
{/block}
{block name='content'}
<body onload="Login.onLoad()">
  <div class="login-content">
      {include file="login_form.tpl"}
  </div>
  <div class="logout-content">
      {include file="logout.tpl"}
  </div>
  <div id="table-home">
    <div class="col-md-4 table-col"></div>
    <div class="login col-md-4 table-col">
      <table id="log" name="log">
        <thead>
          <th>Email</th>
          <th>Type</th>
        </thead>
        <tbody id="tbody-log">
        </tbody>
      </table>
    </div>
    <div class="col-md-4 table-col"></div>
  </div>
</body>
{/block}

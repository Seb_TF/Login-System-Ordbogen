<?php
namespace www\api;

use \php\rpc\StaticDynamicRpcService as Service;

require_once $_SERVER['DOCUMENT_ROOT'].'/ordbogen/www/init_www.php';

// Published www classes
Service::publishClass('\\www\\api\\endpoint\\Login', 'Login.');


// Allow exception flow
Service::setProperty(\php\rpc\AbstractRpcService::PROPERTY_ALLOW_EXCEPTIONS,
true);

// Handle request
Service::handleRequest();

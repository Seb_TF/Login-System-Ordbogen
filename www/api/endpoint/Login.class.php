<?php
namespace www\api\endpoint;

use \php\login as pl;

class Login
{
  /** adds user if there is no other user with that email,
   * if there is a user with the same email it checkes if the password is the samme
   * @param array[] $data
   * @throws Exception
   */
    public static function addUser($data)
    {
        $smarty = new \Smarty;
        $user = new pl\Login();
        if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
          throw new \Exception("Indtast en gyldig email");
        }
        $where = '`'.$user::COLUMN_EMAIL.'` = \''.$data['email'].'\'';
        $usersWhere = $user::getOne($where);
        if ($usersWhere->email != null) {
            if (password_verify($data['password'], $usersWhere->password)) {
                $html = '<tr><td>' . $data['email'] . '</td><td>Login</td></tr>';
                return array('message' => 'Du er nu logget ind', 'html' => $html);
            }
            else {
                throw new \Exception('Koden er forkert');
            }
        } elseif ($usersWhere->email == null) {
          $password_hash = password_hash($data['password'], PASSWORD_DEFAULT);
          $user->email = $data['email'];
          $user->password = $password_hash;

          $user->save();
          $html = '<tr><td>' . $data['email'] . '</td><td>Oprettet</td></tr>';
          return array('message' => 'Du har nu oprettet en bruger', 'html' => $html);
        }
    }

    public static function logout($user)
    {
      $html = '<tr><td>' . $user . '</td><td>Logget ud</td></tr>';
      return array('message' => 'Du er nu logget ud', 'html' => $html);
    }
}

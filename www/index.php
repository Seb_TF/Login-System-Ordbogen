<?php
namespace www;

use \php\login\Login;

require_once($_SERVER['DOCUMENT_ROOT'].'/ordbogen/www/init_www.php');

$users = Login::getAll();

$smarty = new \Smarty;

$smarty->setTemplateDir('/MAMP/htdocs/ordbogen/www/templates/');

$smarty->display('index.tpl');

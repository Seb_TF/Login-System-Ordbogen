<?php
namespace rpcclient;

require_once('JsonRpcClientException.class.php');

/**
  * JSON-RPC client
  *
  * The client supports JSON-RPC version 2.0, 1.1 and 1.0.
  * By default, JSON-RPC version 2.0 is used, but if the server responds with a lower version,
  * the client will adjust
  */
class JsonRpcClient
{
    const VERSION_10 = '1.0';
    const VERSION_11 = '1.1';
    const VERSION_20 = '2.0';

    /**
      * @var string
      */
    private $transport;

    /**
      * @var string
      */
    private $version;

    /**
      * @var string
      */
    private $encoding;

    /**
      * Create a new JSON-RPC client
      * @param RpcTransport $transport
      * @param string $encoding Character encoding. If NULL, mb_internal_encoding is used
      * @param string $version RPC version
      */
    public function __construct(RpcTransport $transport, $encoding = null, $version = self::VERSION_20)
    {
        $this->transport = $transport;
        $this->encoding = $encoding;

        switch ($version) {
            case self::VERSION_10:
            case self::VERSION_11:
            case self::VERSION_20:
                $this->version = $version;
                break;

            default:
                $this->version = self::VERSION_20;
                break;
        }
    }

    /**
      * @param string $data
      * @return string
      */
    private function postData($data)
    {
        if ($this->transport === null) {
            throw new RpcTransportException(RpcTransportException::ERROR_NO_TRANSPORT);
        }

        $this->transport->sendMessage($data);

        return $this->transport->receiveMessage($contentType);
    }

    /**
      * @param mixed $response
      */
    private function adjustVersion($response)
    {
        if (!is_array($response)) {
            $this->version = self::VERSION_10;
        } elseif (array_key_exists('jsonrpc', $response) && $response['jsonrpc'] == '2.0') {
            $this->version = self::VERSION_20;
        } elseif (array_key_exists('version', $response) && $response['version'] == '1.1') {
            $this->version = self::VERSION_11;
        } else {
            $this->version = self::VERSION_10;
        }
    }

    /**
      * @param mixed $data
      * @param string $from
      * @param string $to
      * @return mixed
      */
    private static function convert($value, $from, $to)
    {
        if (is_string($value)) {
            return iconv($from, $to, $value);
        } elseif (is_array($value)) {
            $output = array();
            foreach ($value as $name => $subValue) {
                $output[self::convert($name, $from, $to)] = self::convert($subValue, $from, $to);
            }
            return $output;
        } elseif (is_object($value)) {
            $output = new \stdClass();
            foreach (get_object_vars($value) as $name => $subValue) {
                $key = self::convert($name, $from, $to);
                $output->$key = self::convert($subValue, $from, $to);
            }
            return $output;
        } else {
            return $value;
        }
    }

    /**
      * @param mixed $data
      * @return mixed
      */
    private function encode($data)
    {
        if ($this->encoding === null) {
            return self::convert($data, mb_internal_encoding(), 'UTF-8');
        } else {
            return self::convert($data, $this->encoding, 'UTF-8');
        }
    }

    /**
      * @param mixed $data
      * @return mixed
      */
    private function decode($data)
    {
        if ($this->encoding === null) {
            return self::convert($data, 'UTF-8', mb_internal_encoding());
        } else {
            return self::convert($data, 'UTF-8', $this->encoding);
        }
    }

    /**
      * @param string $name Method name
      * @param array $arguments Arguments
      */
    public function __call($name, array $arguments)
    {
        $request = array();
        if ($this->version == self::VERSION_11) {
            $request['version'] = '1.1';
        } elseif ($this->version == self::VERSION_20) {
            $request['jsonrpc'] = '2.0';
        }

        $request['method'] = $name;
        $request['params'] = $this->encode($arguments);
        $request['id'] = mt_rand();

        $data = json_encode($request);

        $data = $this->postData($data);

        if ($data === false) {
            throw new JsonRpcClientException('Request failed');
        }

        $response = json_decode($data, true);

        $this->adjustVersion($response);

        if (!is_array($response)) {
            throw new jsonRpcClientException('Invalid response from server');
        }

        if (array_key_exists('error', $response)) {
            $message = 'Unknown server error';
            $code = 0;
            if (is_array($response['error'])) {
                if (array_key_exists('message', $response['error'])) {
                    $message = (string)$this->decode($response['error']['message']);
                }
                if (array_key_exists('code', $response['error'])) {
                    $code = (int)$response['error']['code'];
                }
            }
            throw new JsonRpcClientException($message, $code);
        } elseif (array_key_exists('result', $response)) {
            return $this->decode($response['result']);
        } else {
            throw new JsonRpcClientException('Invalid response from server');
        }
    }
}

<?php
namespace php\login;

class Login extends LoginSystem
{
	const TABLE = 'login';

	const COLUMN_PRIMARY_KEY = 'id';
	const COLUMN_EMAIL = 'email';
	const COLUMN_PASSWORD = 'password';
}
